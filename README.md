# Fritzing Parts Bins

#### The parts are mainly for the repository: NodeMCU V3 Docking Board
* As mentioned in the "NodeMCU V3 Docking Board", the decision to stick with Fritzing is for more designers to be able to alter the board.
* Parts:
    * TYPE-C
        * Update 20190729
            * Migrating part into eagle
        * TYPE-CF-6P-RAT-L6.8W [(eg purchase link)](https://detail.tmall.com/item.htm?spm=a230r.1.14.18.1b1256ceHaMSjD&id=580366058155&ns=1&abbucket=7&skuId=4033897187934)
          * This component isn't like standard type-c connector with full functions (and 16+ pins)
        * Like mentioned in [This post](http://forum.fritzing.org/t/usb-3-1-type-c-female-socket-connector/5338/12), it's unrealistic for use in prototyping art/installation project, as even the board can be fabricated, it will need specific equipment to solder.
        * Disclaimer: I'm not in anyway affiliated with the manufacturer of the part.
        * The mechanical dimention on the page is clear so chose the part.
        * Very obviously , it will only have powering functions as none of the DATA pins are connected, treat it functionally like a power port.
            * Pins: A12/B12 (GND) A9/B9 (VBus) A5/B5(CC1,CC2), [function ref](https://www.silabs.com/community/mcu/8-bit/knowledge-base.entry.html/2016/09/26/what_s_the_role_ofc-kQYe)
        * Why if it's no data? As type-c becomes standard, increasing amount of people will likely carry a phone charging cable that's type-c, which means no specific cable need to go with the board/easier to find spare cable.
        * Known problem:
            * There's a mid island which has ghost connections ( pending fix )